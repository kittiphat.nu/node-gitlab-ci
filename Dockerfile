FROM node:12.18.3-alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Setup the environment
ENV PATH /usr/src/app/bin:$PATH

# Bundle app source
COPY . /usr/src/app

# Install app dependencies and build static assets.
RUN npm ci

# Ensure the runtime of the container is in production mode.
ENV NODE_ENV production

CMD ["npm", "run", "start"]